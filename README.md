# RocketShoes

> A shopping cart with React, Redux and Saga.

It is a small shopping cart app that uses React, Redux and Saga.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

The project can be built with npm or yarn, so choose one of the approach bellow in case you don't have any installed on your system.

* **Npm** is distributed with Node.js which means that when you download Node.js, you automatically get npm installed on your computer. [Download Node.js](https://nodejs.org/en/download/)

* **Yarn** is a package manager built by Facebook Team and seems to be faster than npm in general.  [Download Yarn](https://yarnpkg.com/en/docs/install)

### Installing

To download the project follow the instructions bellow:

```
1. git clone https://github.com/diegomais/rocketshoes.git
2. cd rocketshoes
```

* Install and start the API server

```
3. yarn install
4. yarn mock:api
```

* In another terminal window, start the front-end

```
5. yarn start
```

## Author

Diego Mais
* [diegomais@live.com](mailto:diegomais@live.com)
* [diegomais.github.io](http://diegomais.github.io)
* [github.com/diegomais](http://github.com/diegomais)
* [linkedin.com/in/diegomais](http://linkedin.com/in/diegomais)

 ## License

  [MIT](LICENSE)
